package net.demurgos.spaceblob.controls;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

public class StickyKeyListener implements KeyListener {

	/*
     *
	 * EventListener for keyboard inputs Stores the state of the keys to access
	 * it outside of the events.
	 */

    public HashMap<Integer, Boolean> map = new HashMap<Integer, Boolean>();

    public void keyPressed(KeyEvent e) {
        this.map.put(e.getKeyCode(), true);
    }

    public void keyReleased(KeyEvent e) {
        this.map.put(e.getKeyCode(), false);
    }

    public void keyTyped(KeyEvent e) {

    }

    public boolean isPressed(int keyCode) {
        return this.map.containsKey(keyCode) && this.map.get(keyCode) == true;
    }

}
