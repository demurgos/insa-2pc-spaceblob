package net.demurgos.spaceblob.controls;

import java.awt.*;
import java.util.HashMap;

/*
 *
 * Original controls (supporting only keyboard)
 *
 */

public class KeyboardControls implements Controls {

    public HashMap<String, Integer> longMap = new HashMap<String, Integer>();

    public StickyKeyListener skl = new StickyKeyListener();

    public void bindTo(Component c) {
        c.addKeyListener(this.skl);
    }

    public void captureControls() {
        int val;
        for (String name : Controls.CONTROL_NAMES) {
            val = this.longMap.containsKey(name) ? this.longMap.get(name) : 0;
            if (this.isActive(name)) {
                this.longMap.put(name, val < 0 ? 1 : val + 1);
            } else {
                this.longMap.put(name, val > 0 ? -1 : val - 1);
            }
        }
    }

    public boolean isActive(String name) {
        switch (name) {
            case "left":
                return skl.isPressed(37); // left
            case "right":
                return skl.isPressed(39); // right
            case "up":
                return skl.isPressed(38); // up
            case "down":
                return skl.isPressed(40); // down
            case "action":
                return skl.isPressed(10) || skl.isPressed(32) || skl.isPressed(88); // enter
            // ||
            // space
            // ||
            // X
            case "pause":
                return skl.isPressed(80); // P
            case "escape":
                return skl.isPressed(27); // escape
            default:
                return false;
        }
    }

    public boolean isActivating(String name) {
        return this.longMap.containsKey(name) && this.longMap.get(name) == 1;
    }

    public boolean isDeactivating(String name) {
        return this.longMap.containsKey(name) && this.longMap.get(name) == -1;
    }

}
