package net.demurgos.spaceblob.controls;

/*
 *
 * Controls interface.
 * Allows controls persistence and state access.
 *
 */

public interface Controls {

    public static final String[] CONTROL_NAMES = {"down", "up", "left",
            "right", "action", "pause", "menu", "fullScreen", "nextLevel"};

    public abstract void captureControls();

    public abstract boolean isActive(String name);

    public abstract boolean isActivating(String name);

    public abstract boolean isDeactivating(String name);

}
