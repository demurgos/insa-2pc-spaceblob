package net.demurgos.spaceblob.layers;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.ImageAsset;
import net.demurgos.spaceblob.core.Layer;

import java.awt.*;
import java.awt.geom.AffineTransform;

/*
 *
 * Used for parallax on background images.
 *
 */

public class BackgroundLayer extends Layer {

    public ImageAsset img;

    public BackgroundLayer(Game game) {
        super(game);
    }

    public void init(ImageAsset backGround) {
        this.img = backGround;
        this.keep();
    }

    public void draw(Graphics2D g) {
        Image img = this.img.getImage();
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        AffineTransform oldMatrix = g.getTransform();
        double x = ll.camX;
        double y = ll.camY;
        double zoom = 1;

        double ratioX;
        if (Game.WIDTH >= ll.width * ll.tileWidth) {
            ratioX = 0;
        } else {
            ratioX = (x - Game.WIDTH / 2)
                    / (ll.width * ll.tileWidth - Game.WIDTH);
        }
        ratioX = ratioX < 0 ? 0 : (ratioX > 1 ? 1 : ratioX);

        double posX = (ll.width * ll.tileWidth) * ratioX - img.getWidth(null)
                * ratioX;

        double ratioY;
        if (Game.HEIGHT >= ll.height * ll.tileHeight) {
            ratioY = 0;
        } else {
            ratioY = (y - Game.HEIGHT / 2)
                    / (ll.height * ll.tileHeight - Game.HEIGHT);
        }
        ratioY = ratioY < 0 ? 0 : (ratioY > 1 ? 1 : ratioY);

        double posy = (ll.height * ll.tileHeight) * ratioY
                - img.getHeight(null) * ratioY;

        g.drawImage(img, (int) posX, (int) posy, null);
        super.draw(g);
        g.setTransform(oldMatrix);
    }

}
