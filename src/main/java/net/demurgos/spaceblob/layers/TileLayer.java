package net.demurgos.spaceblob.layers;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.core.Layer;
import net.demurgos.spaceblob.entities.Tile;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/*
 *
 * A grid of tiles optimized for research and rendering (cached)
 *
 */

public class TileLayer extends Layer {

    public int width, height, tileWidth, tileHeight;
    public Tile[][] grid;
    public BufferedImage cachedImage;
    public BufferedImage imageCache;
    private boolean useImageCache = false;
    private boolean repaintImageCache;

    public TileLayer(Game game) {
        super(game);
    }

    public void init(int width, int height, int tileWidth, int tileHeight) {
        this.width = width;
        this.height = height;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.grid = new Tile[this.width][this.height];
        this.enableImageCache(true);
        this.keep();
    }

    public TileLayer enableImageCache(boolean use) {
        this.useImageCache = use;
        if (use) {
            this.repaintImageCache = true;
        }
        return this;
    }

    public void setTile(int x, int y, Tile tile) {
        Tile old = this.getTile(x, y);
        if (old != null) {
            old.dispose();
        }
        this.grid[x][y] = tile;
        this.add(tile);
    }

    public Tile getTile(int x, int y) {
        return this.grid[x][y];
    }

    public synchronized void draw(Graphics2D g) {
        Graphics2D masterGraphics = g;
        if (this.useImageCache && (this.repaintImageCache || this.viewChanged)) {
            this.imageCache = new BufferedImage(this.width * this.tileWidth,
                    this.height * this.tileHeight, BufferedImage.TYPE_INT_ARGB);
            g = this.imageCache.createGraphics();
        }
        if ((!this.useImageCache) || this.repaintImageCache || this.viewChanged) {
            Tile cur;
            AffineTransform oldMatrix = g.getTransform();
            for (int x = 0; x < this.width; x++) {
                for (int y = 0; y < this.height; y++) {
                    cur = this.grid[x][y];
                    if (cur != null && !cur.isDisposed()) {
                        g.setTransform(oldMatrix);
                        g.translate(x * this.tileWidth, y * this.tileHeight);
                        cur.draw(g);
                    }
                }
            }
            g.setTransform(oldMatrix);
        }

        if (this.useImageCache && this.imageCache != null) {
            masterGraphics.drawImage(this.imageCache, 0, 0, null);
            this.repaintImageCache = false;
        }

    }

    public boolean update(int delta) {
        boolean changed = super.update(delta);
        if (changed) {
            this.cachedImage = null;
        }
        return changed;
    }

}
