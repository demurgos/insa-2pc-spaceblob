package net.demurgos.spaceblob.layers;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.ImageAsset;
import net.demurgos.spaceblob.core.Layer;

import java.awt.*;

/*
 *
 * The main menu
 *
 */

public class Menu extends Layer {

    public static ImageAsset IMG_BACK = new ImageAsset("img/ui/back.png");
    public static ImageAsset IMG_FRONT = new ImageAsset("img/ui/front.png");
    public static ImageAsset IMG_PLAY = new ImageAsset("img/ui/play.png");
    public static ImageAsset IMG_CONTINUE = new ImageAsset(
            "img/ui/continue.png");
    public static ImageAsset IMG_EXIT = new ImageAsset("img/ui/exit.png");
    public static ImageAsset IMG_PLAY_ON = new ImageAsset("img/ui/play_on.png");
    public static ImageAsset IMG_CONTINUE_ON = new ImageAsset(
            "img/ui/continue_on.png");
    public static ImageAsset IMG_EXIT_ON = new ImageAsset("img/ui/exit_on.png");

    public String focused = "continue";
    public double backOffY = 0;

    public Menu(Game game) {
        super(game);
    }

    public boolean update(int delta) {

        this.backOffY = (this.backOffY + 0.1 * delta) % Game.HEIGHT;

        boolean down = this.game.controls.isActivating("down");
        boolean up = this.game.controls.isActivating("up");
        boolean left = this.game.controls.isActivating("left");
        boolean right = this.game.controls.isActivating("right");
        boolean action = this.game.controls.isActivating("action");

        if (down) {
            this.focusNext();
        } else if (up) {
            this.focusPrev();
        } else if (action || right) {
            this.execAction();
        }

        return true;
    }

    public void execAction() {

        if (this.focused.equals("continue")) {
            this.play(this.game.save.getBestLevel());
        } else if (this.focused.equals("play")) {
            this.game.save.reset();
            this.play(0);
        } else if (this.focused.equals("exit")) {
            System.exit(0);
        }
    }

    public void focusNext() {
        if (this.focused.equals("continue")) {
            this.focused = "play";
        } else if (this.focused.equals("play")) {
            this.focused = "exit";
        } else if (this.focused.equals("exit")) {
            this.focused = "continue";
        }
    }

    public void focusPrev() {
        if (this.focused.equals("continue")) {
            this.focused = "exit";
        } else if (this.focused.equals("play")) {
            this.focused = "continue";
        } else if (this.focused.equals("exit")) {
            this.focused = "play";
        }
    }

    public void play(int lvl) {
        this.game.playSolo(lvl);
        this.dispose();
    }

    public void draw(Graphics2D g) {
        g.drawImage(Menu.IMG_BACK.getImage(), 0, -Game.HEIGHT
                + (int) this.backOffY, null);
        g.drawImage(Menu.IMG_BACK.getImage(), 0, (int) this.backOffY, null);
        g.drawImage(Menu.IMG_FRONT.getImage(), 0, 0, null);
        int y = 170;
        int spaceY = 15;
        Image img = (this.focused.equals("continue") ? Menu.IMG_CONTINUE_ON
                : Menu.IMG_CONTINUE).getImage();
        g.drawImage(img, Game.WIDTH / 2 - img.getWidth(null) / 2, y, null);
        y += img.getHeight(null) + spaceY;
        img = (this.focused.equals("play") ? Menu.IMG_PLAY_ON : Menu.IMG_PLAY)
                .getImage();
        g.drawImage(img, Game.WIDTH / 2 - img.getWidth(null) / 2, y, null);
        y += img.getHeight(null) + spaceY;
        img = (this.focused.equals("exit") ? Menu.IMG_EXIT_ON : Menu.IMG_EXIT)
                .getImage();
        g.drawImage(img, Game.WIDTH / 2 - img.getWidth(null) / 2, y, null);
    }
}
