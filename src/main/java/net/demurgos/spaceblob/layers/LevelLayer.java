package net.demurgos.spaceblob.layers;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.core.Layer;
import net.demurgos.spaceblob.entities.Player;
import net.demurgos.spaceblob.entities.Tile;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

/*
 *
 * Root layer containing the entities of a level.
 * Manages camera position as well as wall collision.
 *
 */

public class LevelLayer extends Layer {

    public static int CAM_DELAY = 500;
    public static int CAM_OFFSET = 200;

    public int width, height, tileWidth, tileHeight;
    public Color backgroundColor = new Color(0, 0, 0);
    public double camX;
    public double camY;
    public double camOffX = 0;
    public double camOffY = 0;
    public double camZoom = 1;
    public int camPos = 0;
    public int camPosLeft = 0;
    public int camPosStill = 0;
    public int camPosRight = 0;

    public LevelLayer(Game game) {
        super(game);
    }

    public static int[] toIntCoords(double[] coords) {
        int[] res = new int[2];
        res[0] = (int) Math.floor(coords[0]);
        res[1] = (int) Math.floor(coords[1]);
        return res;
    }

    public void init(int width, int height, int tileWidth, int tileHeight) {
        this.width = width;
        this.height = height;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.keep();
    }

    public List<Tile> getTiles(int x, int y) {
        if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
            return new ArrayList<Tile>();
        }

        List<Tile> res = new ArrayList<Tile>();
        Tile cur;
        List<TileLayer> l = this.getEntities(TileLayer.class);
        for (TileLayer tl : l) {
            cur = tl.getTile(x, y);
            if (cur != null) {
                res.add(cur);
            }
        }
        return res;
    }

    public boolean isTileSolid(int x, int y) {
        List<Tile> tiles = this.getTiles(x, y);
        for (Tile t : tiles) {
            if (t.solid) {
                return true;
            }
        }
        return false;
    }

	/*
     * Updates the position of the camera. The camera has three stable position
	 * (-1, 0, 1) When a threshold is reached, the position is switched & a
	 * transition occurs.
	 */

    public boolean isSolidOnLine(double[] line, int offX, int offY) {
        double[] tiledPos = new double[4];

        tiledPos[0] = this.absToTilesX(Math.min(line[0], line[2]));
        tiledPos[1] = this.absToTilesY(Math.min(line[1], line[3]));
        tiledPos[2] = this.absToTilesX(Math.max(line[0], line[2]));
        tiledPos[3] = this.absToTilesY(Math.max(line[1], line[3]));

        boolean vertical = line[0] == line[2];

        if (tiledPos[2] % 1 == 0 && !vertical) {
            tiledPos[2]--;
        }

        if (tiledPos[3] % 1 == 0 && vertical) {
            tiledPos[3]--;
        }

        int x1 = (int) tiledPos[0];
        int y1 = (int) tiledPos[1];
        int x2 = (int) tiledPos[2];
        int y2 = (int) tiledPos[3];

        if (vertical) {
            for (int y = y1; y <= y2; y++) {
                if (offX == 0 && offY == 0) {
                    if (this.isTileSolid(x1, y)) {
                        return true;
                    }
                } else {
                    if (this.isTileSolid(x1 + offX, y + offY)
                            && !this.isTileSolid(x1, y)) {
                        return true;
                    }
                }
            }
        } else {
            for (int x = x1; x <= x2; x++) {
                if (offX == 0 && offY == 0) {
                    if (this.isTileSolid(x, y1)) {
                        return true;
                    }
                } else {
                    if (this.isTileSolid(x + offX, y1 + offY)
                            && !this.isTileSolid(x, y1)) {
                        return true;
                    }
                }
            }
        }

        return false;

    }

    public boolean update(int delta) {

        if (this.game.controls.isActivating("nextLevel")) {
            this.game.levels.loadNext();
        }

        Player p = this.game.root.getFirstEntity(Player.class);
        if (p != null) {

            if (this.game.controls.isActive("left")
                    || (p.dx < 0 && !this.game.controls.isActive("right"))) {
                this.camPosLeft += delta * (this.camPos > 0 ? 2 : 0.5);
            } else if (this.game.controls.isActive("right")
                    || (p.dx > 0 && !this.game.controls.isActive("left"))) {
                this.camPosRight += delta * (this.camPos < 0 ? 2 : 0.5);
            } else {
                this.camPosStill += delta * (this.camPos != 0 ? 2 : 1);
            }

            if (this.camPosLeft > LevelLayer.CAM_DELAY) {
                this.camPosLeft = 0;
                this.camPosStill = 0;
                this.camPosRight = 0;
                if (this.camPos >= 0) {
                    this.camPos--;
                }
            } else if (this.camPosStill > LevelLayer.CAM_DELAY) {
                this.camPosLeft = 0;
                this.camPosStill = 0;
                this.camPosRight = 0;
                if (this.camPos != 0) {
                    this.camPos = 0;
                }
            } else if (this.camPosRight > LevelLayer.CAM_DELAY) {
                this.camPosLeft = 0;
                this.camPosStill = 0;
                this.camPosRight = 0;
                if (this.camPos <= 0) {
                    this.camPos++;
                }
            }

            double x = this.camPos * LevelLayer.CAM_OFFSET / this.camZoom;

            if (this.camOffX != x) {
                double camSpeed = Math.abs(x - this.camOffX)
                        / LevelLayer.CAM_DELAY;
                if (this.camOffX < x) {
                    this.camOffX += delta * camSpeed;
                    this.camOffX = Math.min(this.camOffX, x);
                } else if (this.camOffX > x) {
                    this.camOffX -= delta * camSpeed;
                    this.camOffX = Math.max(this.camOffX, x);
                }
            }

            this.camX = p.x + this.camOffX;
            this.camY = p.y + this.camOffY;
        }

        return super.update(delta);
    }

    public void draw(Graphics2D g) { // camera follows the player
        double zoom = this.camZoom;
        AffineTransform oldMatrix = g.getTransform();
        g.setPaint(this.backgroundColor);
        g.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);
        double x = this.camX;
        double y = this.camY;
        g.translate((Game.WIDTH) / 2, (Game.HEIGHT) / 2);
        g.translate(-x * zoom, -y * zoom);
		/*
		 * aims to keep the player from going out of the level frame if the
		 * player runs to the left part of the frame, the game window doesn't
		 * follow him if the player runs to the upper part of the frame, the
		 * game window doesn't follow him as well
		 */
        double offsetx = Game.WIDTH / (2 * zoom) - x;
        double offsety = Game.HEIGHT / (2 * zoom) - y;
        if (offsetx >= 0) {
            g.translate(-offsetx * zoom, 0);
        }
        if (offsety >= 0) {
            g.translate(0, -offsety * zoom);
        }

        double offsetxforward = ((this.width) * (this.tileWidth) - (Game.WIDTH / (2 * zoom)))
                - x;
        double offsetyforward = ((this.height) * (this.tileHeight) - (Game.HEIGHT / (2 * zoom)))
                - y;
        if (offsetxforward <= 0) {
            g.translate(-offsetxforward * zoom, 0);
        }
        if (offsetyforward <= 0) {
            g.translate(0, -offsetyforward * zoom);
        }
        g.scale(zoom, zoom);

        super.draw(g);
        g.setTransform(oldMatrix);

    }

    public double absToTilesX(double x) {
        return x / this.tileWidth;
    }

    public double absToTilesY(double y) {
        return y / this.tileHeight;
    }

    public double tilesToAbsX(double x) {
        return x * this.tileWidth;
    }

    public double tilesToAbsY(double y) {
        return y * this.tileHeight;
    }

    public double[] absToTiles(double x, double y) {
        double[] res = new double[2];
        res[0] = absToTilesX(x);
        res[1] = absToTilesY(y);
        return res;
    }

    public double[] tilesToAbs(double x, double y) {
        double[] res = new double[2];
        res[0] = tilesToAbsX(x);
        res[1] = tilesToAbsY(y);
        return res;
    }

}
