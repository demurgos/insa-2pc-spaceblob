package net.demurgos.spaceblob.assets;

import java.awt.*;

/*
 *
 * Represents a font asset. (ttf)
 *
 */

public class FontAsset extends Asset {
    public Font font;

    public FontAsset(String path) {
        super(path);
    }

    public void load(boolean force) {
        if (force || this.font == null) {
            super.load(force);
            try {
                this.font = Font.createFont(Font.TRUETYPE_FONT, this.file);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    public Font getFont() {
        this.load();
        return this.font;
    }

}
