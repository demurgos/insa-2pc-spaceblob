package net.demurgos.spaceblob.assets;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.core.Entity;
import net.demurgos.spaceblob.core.Layer;
import net.demurgos.spaceblob.entities.*;
import net.demurgos.spaceblob.entities.flags.Death;
import net.demurgos.spaceblob.entities.flags.Item;
import net.demurgos.spaceblob.entities.flags.LevelEnd;
import net.demurgos.spaceblob.layers.BackgroundLayer;
import net.demurgos.spaceblob.layers.LevelLayer;
import net.demurgos.spaceblob.layers.TileLayer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.awt.image.BufferedImage;
import java.io.FileReader;
import java.util.*;

/*
 *
 * Represents a level asset (a Tiled level exported to JSON)
 *
 */

public class LevelAsset extends Asset {
    public JSONObject jo;
    public Map<Integer, JSONObject> tileSets;
    public Map<String, SpriteMapAsset> sprites;

    public LevelAsset(String path) {
        super(path);
    }

    public static String getString(JSONObject jo, String name) {
        return (String) jo.get(name);
    }

    public static int getInt(JSONObject jo, String name) {
        int n = 0;
        try {
            n = (int) (long) jo.get(name);
        } catch (Exception e1) {
            try {
                n = (int) (double) jo.get(name);
            } catch (Exception e2) {
                System.err.println("Failed to read int " + name);
                System.err.println(jo);
            }
        }
        return n;
    }

    public static boolean getBoolean(JSONObject jo, String name) {
        return (boolean) jo.get(name);
    }

    public static List<JSONObject> getObjectList(JSONObject jo, String name) {
        return (List<JSONObject>) jo.get(name);
    }

    public static JSONObject getObject(JSONObject jo, String name) {
        return (JSONObject) jo.get(name);
    }

    public static int[][] getTileLayerData(JSONObject jo) {
        List<Object> data = (List<Object>) jo.get("data");
        int width = LevelAsset.getInt(jo, "width");
        int height = LevelAsset.getInt(jo, "height");
        int[][] grille = new int[width][height];
        int i;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                i = x + y * width;
                grille[x][y] = (int) (long) data.get(i);
            }
        }
        return grille;
    }

    public void load(boolean force) {
        if (force || this.jo == null) {
            super.load(force);
            try {
                FileReader reader = new FileReader(this.file);
                JSONParser jsonParser = new JSONParser();
                this.jo = (JSONObject) jsonParser.parse(reader);
            } catch (Exception e) {
                System.err.println("Impossible de lire le niveau " + this.path);
                System.err.println(e);
                System.exit(0);
            }

            try {
                this.loadTileSets();
            } catch (Exception e) {
                System.err
                        .println("Impossible de charger les tilesets associ�s � "
                                + this.path);
                e.printStackTrace();
                System.exit(0);

            }

        }
    }

    public void loadTileSets() {
        this.tileSets = new HashMap<Integer, JSONObject>();
        this.sprites = new HashMap<String, SpriteMapAsset>();
        for (JSONObject ts : LevelAsset.getObjectList(this.jo, "tilesets")) {
            this.tileSets.put(LevelAsset.getInt(ts, "firstgid"), ts);
            String name = LevelAsset.getString(ts, "name");
            String imgpath = LevelAsset.getString(ts, "image");
            String[] parts = imgpath.split("/");
            String filename = parts[parts.length - 1];
            int tilewidth = LevelAsset.getInt(ts, "tilewidth");
            int tileheight = LevelAsset.getInt(ts, "tileheight");
            this.sprites.put(name, new SpriteMapAsset("img/tilesets/"
                    + filename, tilewidth, tileheight));
        }
    }

    public JSONObject getJSON() {
        this.load(false);
        return this.jo;
    }

    public LevelLayer getLevel(Game g) {
        LevelLayer levelLayer = new LevelLayer(g);
        JSONObject levelJSON = this.getJSON();
        levelLayer.init(LevelAsset.getInt(levelJSON, "width"),
                LevelAsset.getInt(levelJSON, "height"),
                LevelAsset.getInt(levelJSON, "tilewidth"),
                LevelAsset.getInt(levelJSON, "tileheight"));
        List<JSONObject> layers = LevelAsset.getObjectList(levelJSON, "layers");
        for (JSONObject layer : layers) {
            String layerType = LevelAsset.getString(layer, "type");
            if (layerType.equals("tilelayer")) {
                levelLayer.add(this.getTileLayer(g, layer));
            } else if (layerType.equals("objectgroup")) {
                levelLayer.add(this.getObjectLayer(g, layer));
            } else if (layerType.equalsIgnoreCase("imagelayer")) {
                levelLayer.add(this.getImageLayer(g, layer));
            } else {
                System.err
                        .println("type de calque non support� : " + layerType);
            }
        }
        Player p = levelLayer.getFirstEntity(Player.class);
        if (p != null) {
            levelLayer.camX = p.x;
            levelLayer.camY = p.y;
        }
        return levelLayer;
    }

    public TileLayer getTileLayer(Game g, JSONObject layer) {
        TileLayer tl = new TileLayer(g);
        int width = LevelAsset.getInt(layer, "width");
        int height = LevelAsset.getInt(layer, "height");
        tl.init(width, height, LevelAsset.getInt(this.getJSON(), "tilewidth"),
                LevelAsset.getInt(this.getJSON(), "tileheight"));
        int[][] grille = LevelAsset.getTileLayerData(layer);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int id = grille[x][y];
                if (id > 0) {
                    tl.setTile(x, y, this.getTile(g, id));
                }
            }
        }
        if (!LevelAsset.getBoolean(layer, "visible")) {
            tl.setVisible(false);
        }

        return tl;
    }

    public Tile getTile(Game g, int id) {
        JSONObject ts = this.getTileSet(id);
        SpriteMapAsset sma = this.sprites.get(LevelAsset.getString(ts, "name"));
        BufferedImage bi = sma
                .getSprite(id - LevelAsset.getInt(ts, "firstgid"));
        Tile t = new Tile(g);
        if (getString(ts, "type") == "solid") {
            t.setSolid(true);
        }

        JSONObject tp = this.getTileProperties(id);
        if (tp != null) {
            if (LevelAsset.getString(tp, "type").equals("solid")) {
                t.setSolid(true);
            }
        }
        t.init(bi);
        return t;
    }

    public JSONObject getTileSet(int id) {
        Set<Integer> firstIdsSet = this.tileSets.keySet();
        List<Integer> firstIdsList = new ArrayList<Integer>(firstIdsSet);
        Collections.sort(firstIdsList);
        int cur;
        for (int i = firstIdsList.size() - 1; i >= 0; i--) {
            cur = firstIdsList.get(i);
            if (id >= cur) {
                return this.tileSets.get(cur);
            }
        }
        System.err.println("Impossible de trouver le tileset pour " + id);
        System.exit(0);
        return null;
    }

    public JSONObject getTileProperties(int id) {
        JSONObject ts = this.getTileSet(id);
        int tileId = id - getInt(ts, "firstgid");

        if (!ts.containsKey("tileproperties")) {
            return null;
        }

        JSONObject tileProperties = LevelAsset.getObject(ts, "tileproperties");

        if (!tileProperties.containsKey("" + tileId)) {
            return null;
        }

        return LevelAsset.getObject(tileProperties, "" + tileId);
    }

    public Layer getObjectLayer(Game g, JSONObject layer) {
        Layer l = new Layer(g);
        List<JSONObject> objects = LevelAsset.getObjectList(layer, "objects");
        Entity ent;
        for (JSONObject obj : objects) {
            ent = this.getEntity(g, obj);
            if (ent != null) {
                l.add(ent);
            }
        }
        l.keep();
        return l;
    }

    public BackgroundLayer getImageLayer(Game g, JSONObject layer) {
        BackgroundLayer l = new BackgroundLayer(g);
        String imgpath = LevelAsset.getString(layer, "image");
        String[] parts = imgpath.split("/");
        String filename = parts[parts.length - 1];

        ImageAsset img = new ImageAsset("img/background/" + filename);

        l.init(img);

        return l;
    }

    public Entity getEntity(Game g, JSONObject obj) {
        String name = LevelAsset.getString(obj, "name");
        int x = 0;
        try {
            x = LevelAsset.getInt(obj, "x");
        } catch (Exception e) {
            System.err.println(obj);
        }

        int y = LevelAsset.getInt(obj, "y");
        int w = LevelAsset.getInt(obj, "width");
        int h = LevelAsset.getInt(obj, "height");
        switch (name) {
            case "player":
                Player p = new Player(g);
                p.init((2 * x + w) / 2, (2 * y + h) / 2);
                return p;

            case "death":
                Death d = new Death(g);
                d.init(x, y, w, h);
                return d;

            case "end":
                LevelEnd l = new LevelEnd(g);
                l.init(x, y, w, h);
                return l;

            case "item":
                Item i = new Item(g);
                i.init(x, y, w, h);
                return i;

            case "ghost":
                Ghost f = new Ghost(g);
                f.init((2 * x + w) / 2, (2 * y + h) / 2);
                return f;
            case "sentinelle":
                Sentinelle s = new Sentinelle(g);
                s.init(x, y, w, h);
                return s;

            case "tower":
                MissileLauncher ml = new MissileLauncher(g);
                ml.init((2 * x + w) / 2, (2 * y + h) / 2);
                return ml;

        }
        return null;
    }

}
