package net.demurgos.spaceblob.assets;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/*
 *
 * Represents a save file.
 * Allows to write changes on the disk as a JSON file.
 *
 */

public class SaveAsset extends Asset {
    public JSONObject jo;
    public int deaths;
    public List<Boolean> levels = new ArrayList<Boolean>();
    public List<Boolean> items = new ArrayList<Boolean>(); // unused

    public SaveAsset(String path) {
        super(path);
    }

    public void load(boolean force) {
        if (force || this.jo == null) {
            if (!(new File(Asset.getAbsPath(this.path))).exists()) {
                this.save();
            }
            super.load(force);
            try {
                FileReader reader = new FileReader(this.file);
                JSONParser jsonParser = new JSONParser();
                this.jo = (JSONObject) jsonParser.parse(reader);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }
            this.deaths = (int) (long) this.jo.get("deaths");
            List<JSONObject> ll = (List<JSONObject>) jo.get("levels");
            this.levels = new ArrayList<Boolean>();
            for (Object o : ll) {
                this.levels.add((boolean) o);
            }
        }
    }

    public boolean isLevelDone(int n) {
        this.load();
        return n >= this.levels.size() ? false : this.levels.get(n);
    }

    public int getBestLevel() {
        this.load();
        int best = 0;
        while (this.levels.size() > best + 1 && this.levels.get(best + 1)) {
            best++;
        }
        return best;
    }

    public void setLevelDone(int n, boolean done) {
        this.load();
        while (this.levels.size() <= n) {
            this.levels.add(false);
        }
        this.levels.set(n, done);
        this.save();
    }

    public int getDeaths() {
        this.load();
        return this.deaths;

    }

    public void setDeaths(int n) {
        this.load();
        this.deaths = n;
        this.save();
    }

    public void addDeath() {
        this.load();
        this.setDeaths(this.deaths + 1);
    }

    public void reset() {
        this.load();
        this.deaths = 0;
        this.levels = new ArrayList<Boolean>();
        this.save();
    }

    @SuppressWarnings("unchecked")
    public void save() {

        JSONObject obj = new JSONObject();
        obj.put("deaths", this.deaths);
        obj.put("levels", this.levels);
        obj.put("items", this.items);

        try {
            PrintWriter writer = new PrintWriter(Asset.getAbsPath(this.path),
                    "UTF-8");
            writer.println(obj.toString());
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
