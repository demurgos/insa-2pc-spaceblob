package net.demurgos.spaceblob.assets;

import java.awt.image.BufferedImage;

/*
 *
 * Represents a spriteMap asset.
 * It's an image asset representing a grid of smaller images (sprites).
 * Sprites are identified by their position on the grid:
 * #####
 * #012#
 * #345#
 * #678#
 * #####
 *
 */

public class SpriteMapAsset extends ImageAsset {

    public BufferedImage[] sprites;
    public int spriteWidth;
    public int spriteHeight;

    public SpriteMapAsset(String path, int spriteWidth, int spriteHeight) {
        super(path);
        this.spriteWidth = spriteWidth;
        this.spriteHeight = spriteHeight;
    }

    public void load(boolean force) {
        if (force || this.sprites == null) {
            super.load(force);
            BufferedImage map = this.image;
            int width = map.getWidth();
            int height = map.getHeight();
            if (this.spriteWidth == 0 || this.spriteHeight == 0) {
                System.out.println("Missing sprite dimensions");
                System.exit(0);
            } else if (width % this.spriteWidth != 0
                    || height % this.spriteHeight != 0) {
                System.out.println("Sprite map does not fit sprite dimensions");
                System.exit(0);
            }
            int numX = width / this.spriteWidth;
            int numY = height / this.spriteHeight;
            this.sprites = new BufferedImage[numX * numY];
            int i = 0;
            for (int y = 0; y < numY; y++) {
                for (int x = 0; x < numX; x++) {
                    this.sprites[i] = map.getSubimage(x * this.spriteWidth, y
                                    * this.spriteHeight, this.spriteWidth,
                            this.spriteHeight);
                    i++;
                }
            }
        }

    }

    public BufferedImage getSprite(int i) {
        this.load(false);
        return this.sprites[i];
    }

}
