package net.demurgos.spaceblob.assets;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

/*
 *
 * Represents an image asset.
 *
 */

public class ImageAsset extends Asset {
    public BufferedImage image;

    public ImageAsset(String path) {
        super(path);
    }

    public void load(boolean force) {
        if (force || this.image == null) {
            super.load(force);
            try {
                Image tmpImage = ImageIO.read(this.file);

                if (tmpImage instanceof BufferedImage) {
                    this.image = (BufferedImage) tmpImage;
                    return;
                }

                this.image = new BufferedImage(tmpImage.getWidth(null),
                        tmpImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);

                Graphics2D g = this.image.createGraphics();
                g.drawImage(this.image, 0, 0, null);
                g.dispose();

            } catch (Exception err) {
                System.err.println("Unable to load image " + this.path);
                System.exit(0);
            }
        }
    }

    public Image getImage() {
        this.load();
        return this.image;
    }

}
