package net.demurgos.spaceblob.assets;

import java.io.File;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/*
 *
 * Represents a resource on the disk. Either loaded or not.
 * Allows to load everything at the beginning of the program or at the last moment.
 *
 */

public class Asset {
    public static final String root = "/net/demurgos/spaceblob/assets/";
    public static Map<String, Asset> index = new HashMap<String, Asset>();
    public String path = null;
    public File file = null;

    public Asset() {
    }

    public Asset(String path) {
        this.path = path;
        Asset.register(this);
    }

    public static Asset getAsset(String path) {
        return Asset.index.containsKey(path) ? Asset.index.get(path) : null;
    }

    public static ImageAsset getImageAsset(String path) {
        return Asset.index.containsKey(path) ? (ImageAsset) Asset.index
                .get(path) : null;
    }

    public static FontAsset getFontAsset(String path) {
        return Asset.index.containsKey(path) ? (FontAsset) Asset.index
                .get(path) : null;
    }

    public static SoundAsset getSoundAsset(String path) {
        return Asset.index.containsKey(path) ? (SoundAsset) Asset.index
                .get(path) : null;
    }

    public static SpriteMapAsset getSpriteMapAsset(String path) {
        return Asset.index.containsKey(path) ? (SpriteMapAsset) Asset.index
                .get(path) : null;
    }

    public static void register(Asset o) {
        Asset.index.put(o.path, o);
    }

    public static void loadAll(boolean force) {
        for (Entry<String, Asset> e : index.entrySet()) {
            Asset value = e.getValue();
            value.load(force);
        }
    }

    public static void loadAll() {
        Asset.loadAll(false);
    }

    public static String getAbsPath(String relPath) {
        String path = "";
        try {
            if (relPath == null) {
                throw new Exception("Cannot load asset with null path");
            }
            URL url = Asset.class.getResource(Asset.root + relPath);
            if (url == null) {
                throw new Exception("Cannot load asset " + Asset.root + relPath);
            }
            path = URLDecoder.decode(url.getPath(), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        return path;
    }

    public static void writeFile(String path, String content) {
        PrintWriter writer;
        try {
            writer = new PrintWriter(path, "UTF-8");
            writer.print(content);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

    }

    public File getFile() {
        this.load();
        return this.file;
    }

    public void load(boolean force) {
        if (force || this.file == null) {
            this.file = new File(Asset.getAbsPath(this.path));
        }
    }

    public void load() {
        this.load(false);
    }
}
