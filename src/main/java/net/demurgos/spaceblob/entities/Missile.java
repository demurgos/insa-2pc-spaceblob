package net.demurgos.spaceblob.entities;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.SpriteMapAsset;
import net.demurgos.spaceblob.core.Physical;
import net.demurgos.spaceblob.core.Point;

import java.awt.*;
import java.awt.geom.AffineTransform;

/*
 *
 * A missile with to states:
 * - inactive (just aims at the player)
 * - shot (follows the player)
 *
 */

public class Missile extends Physical {

    public static SpriteMapAsset SPRITE_MAP = new SpriteMapAsset(
            "img/entities/missile.png", 48, 16);
    public static double SPEED = 0.3;
    public static double SPEED_LIMIT = 5;
    public static double FRICTION = 0.0000001;
    public static double AC = 0.001;
    public static int ROTATION_DIST = 12;

    public double direction = 0;
    public boolean shot = false;

    public Missile(Game game) {
        super(game);
    }

    public Missile init(double x, double y) { // specific properties
        this.setView(Missile.SPRITE_MAP);
        this.addAnimation("right", new int[]{0});

        this.x = x;
        this.y = y;
        this.w = 10;
        this.h = 10;
        this.offX = -this.w / 2;
        this.offY = -this.h / 2;
        this.imgX = -39;
        this.imgY = -16 / 2;
        this.keep();
        return this;
    }

    public Missile setDir(double theta) { // angular orientation
        this.direction = theta;
        return this;
    }

    // Entities are designed to be aligned on axis. The missiles rotation feels
    // a bit hack'y :/
    public void draw(Graphics2D g) {
        AffineTransform oldMatrix = g.getTransform();
        double tmpX = this.x;
        double tmpY = this.y;
        g.translate(this.x, this.y);
        g.rotate(this.direction);
        this.x = 0;
        this.y = 0;

        super.draw(g);
        g.setTransform(oldMatrix);
        this.x = tmpX;
        this.y = tmpY;
    }

    public void updateDir() {
        this.setDir(new Point(this.dx, this.dy).getArg());
    }

    public boolean update(int delta) { // follows the player
        if (this.shot) {
            Player p = this.game.root.getFirstEntity(Player.class);
            double[] bounds = p.getBounds();
            double deltaX = (bounds[0] + bounds[2]) / 2 - this.x;
            double deltaY = (bounds[1] + bounds[3]) / 2 - this.y;
            double dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
            Point vel = new Point(this.dx, this.dy);

            if (vel.getMod() > SPEED_LIMIT) {
                vel.setPolar(SPEED_LIMIT, vel.getMod());
            }

            this.dx = vel.getX();
            this.dy = vel.getY();

            Point f;
            if (vel.getMod() > SPEED) {
                f = vel.clone().mul(-FRICTION).unit().mul(vel.getMod() - SPEED)
                        .div(delta);
            } else {
                f = new Point(0, 0);
            }

            this.ddx = deltaX / dist * AC + f.getX();
            this.ddy = deltaY / dist * AC + f.getY();

            if (this.collide(p)) { // kills the player
                p.kill();
            }

        }

        this.move(-20);
        super.update(delta);
        this.move(20);

        if (this.shot) {
            this.updateDir();
        }
        return false;
    }

    public void onHit() {
        this.move(20);
        this.dispose();
        if (this.parent != null) {
            Explosion exp = new Explosion(this.game);
            exp.init(this.x, this.y);
            this.parent.add(exp);
        }
    }

}
