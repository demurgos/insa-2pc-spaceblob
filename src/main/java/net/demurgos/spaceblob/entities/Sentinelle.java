package net.demurgos.spaceblob.entities;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.SpriteMapAsset;
import net.demurgos.spaceblob.core.Mobile;

/*
 *
 * A purple (evil) blob walking left & right & left & right & ...
 *
 */

public class Sentinelle extends Mobile {

    public static SpriteMapAsset SPRITE_MAP = new SpriteMapAsset(
            "img/entities/sentinelle.png", 64, 64);

    public static double SPEED = 0.1;

    public double boxX1;
    public double boxY1;
    public double boxX2;
    public double boxY2;
    public double direction = 1;

    public Sentinelle(Game game) {
        super(game);
    }

    public Sentinelle init(int x, int y, int w, int h) { // initializes
        // Sentinelle's
        // attributes
        this.setView(Sentinelle.SPRITE_MAP);
        this.addAnimation("run_left", new int[]{3, 2, 1, 0});
        this.addAnimation("run_right", new int[]{7, 6, 5, 4});
        this.frameDelay = (int) (Game.TICK_DELAY * 4);
        this.boxX1 = x;
        this.boxY1 = y;
        this.boxX2 = x + w;
        this.boxY2 = y + h;
        this.w = 22;
        this.h = 22;
        this.offX = -this.w / 2;
        this.offY = -this.h / 2;
        this.imgX = -32;
        this.imgY = -32;
        this.x = this.boxX1 - this.offX;
        this.y = this.boxY2;
        this.play("run_right");
        this.keep();
        return this;
    }

    public boolean update(int delta) {
        Player p = this.game.root.getFirstEntity(Player.class);

        double[] bounds = this.getBounds();

        if (bounds[0] < this.boxX1) {
            direction = 1;
            this.play("run_right");
        }
        if (bounds[2] >= this.boxX2) {
            direction = -1;
            this.play("run_left");
        }

        this.dx = Sentinelle.SPEED * direction;
        super.update(delta);

        if (this.collide(p)) {
            p.kill();
        }

        return false;

    }

}
