package net.demurgos.spaceblob.entities;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.SpriteMapAsset;
import net.demurgos.spaceblob.core.Animated;
import net.demurgos.spaceblob.core.Point;

/*
 *
 * An entity that creates missiles and release them periodically.
 *
 */

public class MissileLauncher extends Animated {

    public static SpriteMapAsset SPRITE_MAP = new SpriteMapAsset(
            "img/entities/missileLauncher.png", 32, 32);
    public static int DELAY = 2000;
    public static double START_SPEED = 0.5;
    public double orientation;
    public int timer;
    public Missile nextMissile;

    public MissileLauncher(Game game) {
        super(game);
    }

    public MissileLauncher init(int x, int y) { // specific properties
        // initialization

        this.setView(MissileLauncher.SPRITE_MAP);
        this.addAnimation("right", new int[]{0});

        this.x = x;
        this.y = y;
        this.w = 32;
        this.h = 32;
        this.offX = -this.w / 2;
        this.offY = -this.h / 2;
        this.imgX = -16;
        this.imgY = -16;
        this.orientation = 0;
        this.prepareNextShoot();

        this.keep();
        return this;
    }

    public void prepareNextShoot() { // a delay is set between each shoot
        if (this.parent != null) {
            this.timer = MissileLauncher.DELAY;
            this.nextMissile = new Missile(this.game);
            this.nextMissile.init(this.x, this.y);
            this.parent.add(this.nextMissile);
            this.updateMissile();
        }
    }

    public void updateMissile() {
        Player p = this.game.root.getFirstEntity(Player.class);

        if (p != null) { // the missile targets the player
            double[] center = p.getCenter();
            Point dir = new Point(center[0], center[1]).sub(this.x, this.y)
                    .unit();
            this.nextMissile.setDir(dir.getArg());
            dir.mul(Missile.ROTATION_DIST).add(this.x, this.y);
            this.nextMissile.x = dir.getX();
            this.nextMissile.y = dir.getY();
        }
    }

    public void shoot() {
        if (this.nextMissile != null) {
            this.nextMissile.shot = true;
            Point p = new Point(START_SPEED, this.nextMissile.direction, true);
            this.nextMissile.dx = p.getX();
            this.nextMissile.dy = p.getY();
            // this.nextMissile.move(-100);
            this.nextMissile = null;

        }

    }

    public boolean update(int delta) {
        this.timer -= delta;

        if (this.timer <= 0) {
            this.shoot();
        }
        if (this.nextMissile == null) {
            this.prepareNextShoot();
        } else {
            this.updateMissile();
        }

        return false;
    }

}
