package net.demurgos.spaceblob.entities;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.SpriteMapAsset;
import net.demurgos.spaceblob.core.Animated;

/*
 *
 * A simple entity displaying an explosion.
 *
 */

public class Explosion extends Animated {

    public static SpriteMapAsset SPRITE_MAP = new SpriteMapAsset(
            "img/entities/explosion.png", 59, 59);

    public Explosion(Game game) {
        super(game);
    }

    public Explosion init(double x, double y) {
        this.setView(Explosion.SPRITE_MAP);
        this.addAnimation("explosion",
                new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        this.x = x;
        this.y = y;
        this.imgX = -30;
        this.imgY = -30;
        this.frameDelay = (int) (Game.TICK_DELAY * 2);
        this.disposeOnAnimEnd = true;
        this.keep();
        return this;
    }

    public boolean update(int delta) {
        super.update(delta);
        return true;
    }

}
