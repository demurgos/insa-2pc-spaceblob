package net.demurgos.spaceblob.entities;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.SpriteMapAsset;
import net.demurgos.spaceblob.core.Mobile;

/*
 *
 * An entity that follow the player if he's in the ghost's DETECTION_RADIUS.
 * Kills the player if collision occurs.
 *
 */

public class Ghost extends Mobile {

    public static SpriteMapAsset SPRITE_MAP = new SpriteMapAsset(
            "img/entities/ghost.png", 64, 64);
    public static double SPEED = 0.15;
    public static double DETECTION_RADIUS = 300;

    public Ghost(Game game) {
        super(game);
    }

    public Ghost init(int x, int y) {
        this.setView(Ghost.SPRITE_MAP);
        this.addAnimation("left", new int[]{1});
        this.addAnimation("right", new int[]{0});

        this.x = x;
        this.y = y;
        this.w = 24;
        this.h = 24;
        this.offX = -this.w / 2;
        this.offY = -this.h / 2;
        this.imgX = -32;
        this.imgY = -32;
        this.keep();
        return this;
    }

    public boolean update(int delta) {

        Player p = this.game.root.getFirstEntity(Player.class);
        double[] center = p.getCenter();
        double deltaX = center[0] - this.x;
        double deltaY = center[1] - this.y;
        double dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        if (dist < DETECTION_RADIUS) {
            this.dx = deltaX / dist * SPEED;
            this.dy = deltaY / dist * SPEED;
        } else {
            this.dx = 0;
            this.dy = 0;
        }

        if (dist < this.w * 2 / 3 + p.w / 2) {
            p.kill();
        }

        if (this.dx > 0) {
            this.play("right");
        } else {
            this.play("left");
        }
        super.update(delta);
        return false;
    }

}
