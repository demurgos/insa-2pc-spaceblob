package net.demurgos.spaceblob.entities;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.core.Entity;

import java.awt.*;
import java.awt.image.BufferedImage;

/*
 *
 * A tile entity containing the information about the image & state (solid or not) of the tile.
 *
 */

public class Tile extends Entity {
    public BufferedImage img;
    public boolean solid = false; // Used for collision ?

    public Tile(Game game) {
        super(game);
    }

    public Tile init(BufferedImage img) {
        this.img = img;
        this.keep();
        return this;
    }

    public Tile setSolid(boolean solid) {
        this.solid = solid;
        return this;
    }

    public void draw(Graphics2D g) {
        if (this.img != null) {
            g.drawImage(this.img, 0, 0, null);
        }
    }

}
