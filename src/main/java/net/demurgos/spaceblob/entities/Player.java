package net.demurgos.spaceblob.entities;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.ImageAsset;
import net.demurgos.spaceblob.assets.SpriteMapAsset;
import net.demurgos.spaceblob.core.Physical;
import net.demurgos.spaceblob.layers.LevelLayer;

/*
 *
 * The player. It's you !
 * Jump intensity, friction, collision, death, animations, etc.
 *
 */

public class Player extends Physical {

    public static ImageAsset SPRITE_MAP = new SpriteMapAsset(
            "img/entities/player3_map.png", 64, 64);// player's attributes

    public static int ACTION_DELAY = 150;
    public static double ACC_X = 0.003;
    public static double AIR_BOOST = 1.1;
    public static double GRAVITY = 0.002;
    public static double SPEED_X = 0.6;
    public static double SPEED_Y = 0.4;
    public static double TERMINAL_VEL_COEFF = 0.90; // rate tolerance to reach
    // limit speed
    public static double FRICTION_X = 0.2; // 0: slides, 1:adheres
    public static int JUMP_TIME = 400;
    // public static double JUMP

    public boolean onGround = false;
    public boolean jumping = false;
    public double jumpingLeft = 0;
    public int dir = 1;

    public Player(Game g) {
        super(g);
    }

    public Player init(int x, int y) {

        this.setView(Player.SPRITE_MAP);

        this.addAnimation("run_left", new int[]{3, 2, 1, 0}); // different
        // player
        // animations
        this.addAnimation("run_right", new int[]{7, 6, 5, 4});
        this.addAnimation("air_tl", new int[]{8, 9});
        this.addAnimation("air_bl", new int[]{10, 11});
        this.addAnimation("air_tr", new int[]{12, 13});
        this.addAnimation("air_br", new int[]{14, 15});
        this.addAnimation("still_left", new int[]{17});
        this.addAnimation("still_right", new int[]{16});
        this.frameDelay = (int) (Game.TICK_DELAY * 4);

        this.w = 22; // 32;
        this.h = 22; // 32;
        this.offX = -this.w / 2;
        this.offY = -this.h;
        this.imgX = -32;
        this.imgY = -32;
        this.x = x;
        this.y = y;
        // this.autoMove = false;
        this.keep();
        return this;
    }

    public boolean update(int delta) { // player's behavior

        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        int margin = 16;
        if ( // if the player gets out of the level bounds, he dies
                bounds[0] > ll.width * ll.tileWidth + margin
                        || bounds[1] > ll.height * ll.tileHeight + margin
                        || bounds[2] < -margin
            // || bounds[3] < - margin // out of bounds, top side
                ) {
            this.kill();
            return false;
        }

        if (this.game.controls.isActive("left")
                || this.game.controls.isActive("right")) {
            int dir = this.game.controls.isActive("right") ? 1 : -1;
            this.dir = dir;
            if (dir * this.dx > Player.SPEED_X * Player.TERMINAL_VEL_COEFF) { // terminal
                // velocity
                this.ddx = 0;
                this.dx = dir * Player.SPEED_X;
            } else {
                if (this.dx * dir > 0) { // same sign
                    this.ddx = Player.ACC_X * (dir * Player.SPEED_X - this.dx);
                } else {
                    this.ddx = Player.ACC_X * dir * Player.SPEED_X;
                }
            }
        } else { // stopping
            if (Math.abs(this.dx) < (1 - Player.TERMINAL_VEL_COEFF)) {
                this.ddx = 0;
                this.dx = 0;
            } else {
                this.ddx = -Player.FRICTION_X * this.dx / delta;
            }
        }
        this.jumpingLeft -= delta;
        if ((this.game.controls.isActive("up") || this.game.controls
                .isActive("action"))
                && (this.onGround || (!this.onGround && jumpingLeft > 0))) {
            this.ddy = 0;
            this.dy = -Player.SPEED_Y * 0.8;
            this.jumping = true;
            if (onGround) {
                jumpingLeft = JUMP_TIME;
            }
            this.onGround = false;

        } else {
            if (this.dy > Player.SPEED_Y * Player.TERMINAL_VEL_COEFF) {
                this.ddy = 0;
                this.dy = Player.SPEED_Y;
            } else {
                this.ddy = Player.GRAVITY;
            }
        }

        this.onGround = false;
        super.update(delta);

        if (!this.onGround) {
            if (this.dy < 0) { // rising
                this.play(this.dir > 0 ? "air_tr" : "air_tl");
            } else {
                this.play(this.dir > 0 ? "air_br" : "air_bl");
            }
        } else if (this.dx != 0) {
            this.play(this.dir > 0 ? "run_right" : "run_left");
        } else {
            this.play(this.dir > 0 ? "still_right" : "still_left");
        }

        return true;
    }

    public void onHitBottom() {
        this.onGround = true;
    }

    public void onHitTop() {
        this.jumpingLeft = 0;
    }

    public void kill() { // if the player is killed, the level is reloaded
        this.game.levels.reload();
        this.game.save.addDeath();
    }

}
