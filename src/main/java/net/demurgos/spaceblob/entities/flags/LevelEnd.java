package net.demurgos.spaceblob.entities.flags;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.core.Collidable;
import net.demurgos.spaceblob.entities.Player;

/*
 *
 * Entity triggering the level change.
 *
 */

public class LevelEnd extends Collidable {

    public LevelEnd(Game game) {
        super(game);
    }

    public LevelEnd init(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.w = width;
        this.h = height;
        this.keep();
        return this;
    }

    public boolean update(int delta) {

        Player p = this.game.root.getFirstEntity(Player.class);
        if (p != null && p.collide(this)) {
            this.game.levels.loadNext();
        }
        return true;
    }
}
