package net.demurgos.spaceblob.entities.flags;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.core.Collidable;
import net.demurgos.spaceblob.entities.Player;

/*
 *
 * Kills the player if he touches this entity. (used for spikes)
 *
 */

public class Death extends Collidable {

    public Death(Game game) {
        super(game);
    }

    public Death init(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.w = width;
        this.h = height;
        this.keep();
        return this;
    }

    public boolean update(int delta) {

        Player p = this.game.root.getFirstEntity(Player.class);
        if (this.collide(p)) {
            p.kill();

        }
        super.update(delta);
        return false;
    }
}
