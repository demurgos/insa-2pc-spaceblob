package net.demurgos.spaceblob.entities.flags;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.core.Collidable;
import net.demurgos.spaceblob.entities.Player;

/*
 *
 * Unused entity.
 * (Could represent a power-up)
 *
 */

public class Item extends Collidable {

    public Item(Game game) {
        super(game);
    }

    public Item init(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.w = width;
        this.h = height;
        this.keep();
        return this;
    }

    public boolean update(int delta) {

        Player p = this.game.root.getFirstEntity(Player.class);
        if (this.collide(p)) {
            // p.GetItem();
        }
        return true;
    }
}
