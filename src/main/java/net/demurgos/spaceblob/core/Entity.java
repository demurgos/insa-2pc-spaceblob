package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;

import java.awt.*;
import java.util.UUID;

/*
 *
 * An abstract Entity.
 * An node in the game tree which can update itself & render its content.
 * An entity cannot be removed instantly from the tree. It has to be marked as disposed and will be collected during the next update loop.
 *
 */

public abstract class Entity {

    public UUID uid;
    public Game game;
    public boolean isVisible = true;
    public Layer parent;
    protected boolean isDisposed = true;

    public Entity(Game game) {
        this.uid = UUID.randomUUID();
        this.game = game;
    }

    public Game getGame() {
        return this.game;
    }

    public boolean update(int delta) {
        return false;
    }

    public void draw(Graphics2D g) {
    }

    public void dispose() {
        this.isDisposed = true;
    }

    public void keep() {
        this.isDisposed = false;
    }

    public void setVisible(boolean visible) {
        this.isVisible = visible;
    }

    public boolean isDisposed() {
        return this.isDisposed;
    }
}
