package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;

/*
 *
 * Automatically updates the speed & the position of the entity with its acceleration & speed.
 *
 */

public class Mobile extends Animated {
    public double dx, dy; // velocity
    public double ddx, ddy; // acceleration
    public double oldX, oldY;
    public double oldDX, oldDY;
    public boolean autoMove = true;

    public Mobile(Game game) {
        super(game);
    }

    public boolean update(int delta) {
        super.update(delta);

        if (this.autoMove) {
            this.speed(delta);
            this.move(delta);
        }

        return true;
    }

    public void move(double dx, double dy) {
        this.oldX = this.x;
        this.oldY = this.y;
        this.x += dx;
        this.y += dy;
    }

    public void move(int delta, double dx, double dy) {
        this.move(delta * dx, delta * dy);
    }

    public void move(int delta) {
        this.move(delta, this.dx, this.dy);
    }

    public void unMove() {
        this.x = this.oldX;
        this.y = this.oldY;
    }

    public void speed(double ddx, double ddy) {
        this.oldDX = this.dx;
        this.oldDY = this.dy;
        this.dx += ddx;
        this.dy += ddy;
    }

    public void speed(int delta, double ddx, double ddy) {
        this.speed(delta * ddx, delta * ddy);
    }

    public void speed(int delta) {
        this.speed(delta, this.ddx, this.ddy);
    }

    public void unSpeed() {
        this.dx = this.oldDX;
        this.dy = this.oldDY;
    }

    public void moveInside(Block ent, int delta) {
        if (!this.inside(ent)) {
            this.move(delta);
        }

        double velX = this.dx * delta;
        double velY = this.dy * delta;

        double[] a = this.getBounds();
        double[] b = ent.getBounds();
        if (a[0] + velX < b[0]) {
            velX = b[0] - a[0];
        } else if (a[2] + velX > b[2]) {
            velX = b[2] - a[2];
        }
        if (a[1] + velY < b[1]) {
            velY = b[1] - a[1];
        } else if (a[3] + velY > b[3]) {
            velY = b[3] - a[3];
        }

        this.move(velX, velY);
    }

}
