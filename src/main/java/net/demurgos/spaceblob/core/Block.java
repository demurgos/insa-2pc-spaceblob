package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;

/*
 *
 * Represents a rectangle in the 2D space.
 * x & y are the main coordinates but the rectangle can be translated with the vector (offX, offY)
 *
 */

public abstract class Block extends Entity {
    public double x, y, w, h; // width & height
    public double offX, offY;

    public Block(Game game) {
        super(game);
    }

    public double[] getBounds() { // [x1, y1, x2, y2]
        return new double[]{this.x + this.offX, this.y + this.offY,
                this.x + this.w + this.offX, this.y + this.h + this.offY};
    }

    public double[] getCenter() {
        double[] bounds = this.getBounds();
        double x = (bounds[0] + bounds[2]) / 2;
        double y = (bounds[1] + bounds[3]) / 2;

        return new double[]{x, y};
    }
}
