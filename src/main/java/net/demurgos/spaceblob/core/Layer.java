package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;

import java.awt.*;
import java.util.*;
import java.util.List;

/*
 *
 * An entity containing a group of entities.
 * Broadcasts update & draw calls to its children.
 * Allows to explore its subtree to find elements of a given class.
 * Exploration result is cached for common calls (getFirstEntity with ie. LevelLayer or Player)
 *
 */

public class Layer extends Entity {

    public List<Entity> children = new ArrayList<Entity>();
    public Map<Class, Entity> entityCache = new HashMap<Class, Entity>();
    protected boolean viewChanged = true;
    private boolean useEntityCache = false;

    public Layer(Game game) {
        super(game);
    }

    public Layer enableEntityCache(boolean use) {
        this.useEntityCache = use;
        return this;
    }

    public synchronized boolean update(int delta) {
        if (this.useEntityCache) {
            this.entityCache.clear();
        }

        boolean changed = false;
        int l = this.children.size();
        Entity cur;
        for (int i = 0; i < l; i++) {
            cur = this.children.get(i);
            if (cur.isDisposed()) {
                this.children.remove(i);
                i--;
                l--;
            } else {
                changed |= cur.update(delta);
            }
        }
        this.viewChanged = changed;
        return changed;
    }

    public synchronized void draw(Graphics2D g) {
        for (Entity ent : this.children) {
            if (ent != null && !ent.isDisposed() && ent.isVisible) {
                ent.draw(g);
            }
        }
    }

    public synchronized void add(Entity ent) {
        this.children.add(ent);
        ent.parent = this;
    }

    @SuppressWarnings("unchecked")
    public <C> List<C> getEntities(Class<C> c, boolean recursive) {
        List<C> res = new ArrayList<C>();
        try {
            for (Entity ent : this.children) {
                if (c.isInstance(ent)) {
                    res.add((C) ent);
                }
                if (recursive && ent instanceof Layer) {
                    res.addAll(((Layer) ent).getEntities(c, true));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        return res;
    }

    public <C> List<C> getEntities(Class<C> c) {
        return this.getEntities(c, false);
    }

    // List<Enemy> = layer.getEntities(Enemy.class);

    @SuppressWarnings("unchecked")
    public <C> C getFirstEntity(Class<C> c) {
        C tmp = null;
        if (this.useEntityCache && this.entityCache.containsKey(c)) {
            tmp = (C) this.entityCache.get(c);
            if (tmp != null) {
                return tmp;
            }
        }
        for (Entity ent : this.children) {
            if (c.isInstance(ent)) {
                tmp = (C) ent;
                break;
            }
            if (ent instanceof Layer) {
                tmp = ((Layer) ent).getFirstEntity(c);
                if (tmp != null) {
                    break;
                }
            }
        }

        if (this.useEntityCache && tmp != null) {
            this.entityCache.put(c, (Entity) tmp);
        }

        return tmp;
    }

    public Entity getByUUID(UUID uid) {
        Entity res;
        for (Entity ent : this.children) {
            if (ent.uid.equals(uid)) {
                return ent;
            }
            if (ent instanceof Layer) {
                res = ((Layer) ent).getByUUID(uid);
                if (res != null) {
                    return res;
                }
            }
        }

        return null;
    }

}
