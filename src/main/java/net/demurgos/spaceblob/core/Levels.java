package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.LevelAsset;
import net.demurgos.spaceblob.layers.LevelLayer;

import java.util.List;

/*
 *
 * A level manager used to navigate the levels.
 *
 */

public class Levels {

    public Game game;
    public List<LevelAsset> levels;
    public int curLevel;

    public Levels(Game game, List<LevelAsset> levels) {
        this.game = game;
        this.levels = levels;
        this.curLevel = 0;
        // super(game);
    }

    public void clear() {
        List<LevelLayer> niveauxActuels = this.game.root
                .getEntities(LevelLayer.class);
        for (LevelLayer ll : niveauxActuels) {
            ll.dispose();
        }
    }

    public void load(int n) {
        if (n >= 0 && n < this.levels.size()) {
            this.clear();
            LevelLayer ll = this.levels.get(n).getLevel(this.game);
            this.game.root.add(ll);
            this.curLevel = n;
            this.game.save.setLevelDone(n, true);
        } else {
            this.clear();
            // System.out.println("gagn�");
            this.reload();
        }
    }

    public void reload() {
        load(curLevel);
    }

    public void loadNext() {
        load(curLevel + 1);
    }

    public void loadPrev() {
        load(curLevel - 1);
    }

    public void loadRandom() {
        int n = ((int) Math.random()) * this.levels.size();
        load(n);
    }

}
