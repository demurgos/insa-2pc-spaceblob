package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.layers.LevelLayer;

/*
 *
 * Detects the collisions with the walls & place the entity outside the wall.
 * onHit & similar events are used in descendant entities
 *
 * The entity is first moved horizontally then vertically.
 * The collision with the walls is checked before & after the movement.
 * The collision checks solid tiles along a straight oriented segment.
 *
 */

public class Physical extends Mobile {

    public Physical(Game g) {
        super(g);
    }

    public boolean update(int delta) {
        boolean oldMove = this.autoMove;
        this.autoMove = false;
        super.update(delta);
        this.autoMove = oldMove;

        this.speed(delta, this.ddx, this.ddy);

        boolean hitX = false;

        if (this.dx < 0) {
            hitX = this.prepareHitLeft(false);
        } else if (this.dx > 0) {
            hitX = this.prepareHitRight(false);
        }

        this.move(delta, this.dx, 0);

        if (hitX) {
            if (this.dx < 0) {
                hitX = this.prepareHitLeft(true);
            } else {
                hitX = this.prepareHitRight(true);
            }
        }

        if (hitX) {
            if (this.dx < 0) {
                this.hitLeft();
            } else {
                this.hitRight();
            }
        }

        boolean hitY = false;

        if (this.dy < 0) {
            hitY = this.prepareHitTop(false);
        } else if (this.dy > 0) {
            hitY = this.prepareHitBottom(false);
        }

        this.move(delta, 0, this.dy);

        if (hitY) {
            if (this.dy < 0) {
                hitY = this.prepareHitTop(true);
            } else {
                hitY = this.prepareHitBottom(true);
            }
        }

        if (hitY) {
            if (this.dy < 0) {
                this.hitTop();
            } else {
                this.hitBottom();
            }
        }

        return true;
    }

    private boolean prepareHitBottom(boolean afterMove) {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        double[] line = new double[4];
        bounds[3] -= bounds[3] % ll.tileHeight == 0 ? ll.tileHeight : 0;
        line[0] = bounds[0];
        line[1] = bounds[3]; // bottom y
        line[2] = bounds[2];
        line[3] = bounds[3];
        return ll.isSolidOnLine(line, 0, afterMove ? 0 : 1);
    }

    private boolean prepareHitTop(boolean afterMove) {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        double[] line = new double[4];
        line[0] = bounds[0];
        line[1] = bounds[1];
        line[2] = bounds[2];
        line[3] = bounds[1]; // top y
        return ll.isSolidOnLine(line, 0, afterMove ? 0 : -1);
    }

    private boolean prepareHitLeft(boolean afterMove) {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        double[] line = new double[4];
        line[0] = bounds[0];
        line[1] = bounds[1];
        line[2] = bounds[0]; // left x
        line[3] = bounds[3];
        return ll.isSolidOnLine(line, afterMove ? 0 : -1, 0);
    }

    private boolean prepareHitRight(boolean afterMove) {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        double[] line = new double[4];
        bounds[2] -= bounds[2] % ll.tileWidth == 0 ? ll.tileWidth : 0;
        line[0] = bounds[2]; // right x
        line[1] = bounds[1];
        line[2] = bounds[2];
        line[3] = bounds[3];

        return ll.isSolidOnLine(line, afterMove ? 0 : 1, 0);
    }

    private void hitBottom() {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        this.y -= bounds[3] % ll.tileHeight;
        this.dy = 0;
        this.onHitBottom();
        this.onHit();
    }

    private void hitTop() {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        this.y -= bounds[1] % ll.tileHeight;
        this.y += ll.tileHeight;
        this.dy = 0;
        this.onHitTop();
        this.onHit();
    }

    private void hitLeft() {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        this.x -= bounds[0] % ll.tileWidth;
        this.x += ll.tileWidth;
        this.dx = 0;
        this.onHitLeft();
        this.onHit();
    }

    private void hitRight() {
        LevelLayer ll = this.game.root.getFirstEntity(LevelLayer.class);
        double[] bounds = this.getBounds();
        this.x -= bounds[2] % ll.tileWidth;
        this.dx = 0;
        this.onHitRight();
        this.onHit();
    }

    public void onHit() {

    }

    public void onHitRight() {

    }

    public void onHitLeft() {

    }

    public void onHitTop() {

    }

    public void onHitBottom() {

    }

}
