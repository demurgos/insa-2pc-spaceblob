package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;

/*
 *
 * Adds collision logic to the Entity
 *
 */

public abstract class Collidable extends Block {

    public boolean collidable = true;
    public boolean hitTestMirror = false;

    public Collidable(Game game) {
        super(game);
    }

    // Checks if the segments [a,b] & [c,d] intersect each other
    public static boolean intersect(double a, double b, double c, double d) {
        if (a > b) {
            return intersect(b, a, c, d);
        }

        if (c > d) {
            return intersect(a, b, d, c);
        }

        if (a > d) {
            return intersect(c, d, a, b);
        }

        return b > c;
    }

    public void dispose() {
        super.dispose();
        this.collidable = false;
    }

    public void keep() {
        super.keep();
        this.collidable = true;
    }

    public boolean hitTest(Collidable ent) {
        double[] a = this.getBounds();
        double[] b = ent.getBounds();
        return Collidable.intersect(a[0], a[2], b[0], b[2])
                && Collidable.intersect(a[1], a[3], b[1], b[3]);
    }

    public boolean collide(Collidable ent) {
        return this.collidable && ent.collidable && this.hitTest(ent)
                && (!ent.hitTestMirror || ent.hitTest(this));
    }

    public boolean inside(Block ent) {
        double[] a = this.getBounds();
        double[] b = ent.getBounds();
        return b[0] <= a[0] && a[2] <= b[2] && b[1] <= a[1] && a[3] <= b[3];
    }

}
