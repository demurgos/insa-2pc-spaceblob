package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.Game;
import net.demurgos.spaceblob.assets.ImageAsset;
import net.demurgos.spaceblob.assets.SpriteMapAsset;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/*
 *
 * Binds a view (either an ImageAsset or a SpriteMapAsset) to the entity.
 * Allows to register animations: a name identifying an array of integers representing the sprite sequences to display.
 *
 */

public class Animated extends Collidable {
    public SpriteMapAsset sprites;
    public ImageAsset image;
    public boolean animated = false;
    public int imgX, imgY; // topLeft corner of the image relative to the main
    // coordinates
    public int frameDelay = (int) Game.TICK_DELAY;
    public int remainder = 0;
    public int frame = 0;
    public String animationName;
    public Map<String, int[]> animations = new HashMap<String, int[]>();
    public boolean disposeOnAnimEnd = false;

    public Animated(Game game) {
        super(game);
    }

    public boolean update(int delta) {
        boolean changed = false;
        if (this.animated) {
            if (this.frame < 0) {
                this.frame = 0;
                this.remainder = 0;
            } else {
                this.remainder += delta;
                while (this.remainder > this.frameDelay) {
                    this.nextFrame();
                    this.remainder -= this.frameDelay;
                }
                changed = true;
            }
        }
        return super.update(delta) || changed;
    }

    public void draw(Graphics2D g) {
        if (this.animated) {
            try {
                int frameId = this.getAnimation()[this.frame];
                BufferedImage sprite = this.sprites.getSprite(frameId);
                g.drawImage(sprite, (int) (this.x + this.imgX),
                        (int) (this.y + this.imgY), null);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }

        } else {
            g.drawImage(this.image.getImage(),
                    (int) (this.x + this.offX + this.imgX), (int) (this.y
                            + this.offY + this.imgY), null);
        }

        super.draw(g);
    }

    public void nextFrame() {
        if (this.animationName == null) {
            return;
        }
        this.frame = (this.frame + 1) % this.getAnimation().length;
        if (this.frame == 0 && this.disposeOnAnimEnd) {
            this.dispose();
        }
    }

    public int[] getAnimation() {
        int[] anim = this.animations.get(this.animationName);
        if (anim == null) {
            System.err.println("Animation not found: " + this.animationName);
        }
        return anim;
    }

    public void setView(ImageAsset image) {
        this.image = image;
    }

    public void addAnimation(String animationName, int[] sprites) {
        this.animated = true;
        this.animations.put(animationName, sprites);
        if (this.image instanceof SpriteMapAsset) {
            this.sprites = (SpriteMapAsset) this.image;
        } else {
            System.err.println("View must be a sprite map asset for " + this);
            System.exit(0);
        }
        if (this.animationName == null) {
            this.animationName = animationName;
        }
    }

    public void play(String animationName) {
        if (this.animationName == null
                || !this.animationName.equals(animationName)) {
            this.animationName = animationName;
            this.frame = 0;
        }
    }

    // addAnimation("jump", {10,11,12,13});
}
