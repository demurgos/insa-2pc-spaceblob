package net.demurgos.spaceblob.core;

import net.demurgos.spaceblob.assets.Asset;
import net.demurgos.spaceblob.assets.SoundAsset;

import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

/*
 *
 * A sound object used by the game.
 *
 */

public class Sound {
    public SoundAsset asset;
    public int resumeAt = 0;
    public boolean loop;

    public Sound() {
    }

    public Sound(String path) {
        this.asset = Asset.getSoundAsset(path);
        if (this.asset == null) {
            this.asset = new SoundAsset(path);
        }
    }

    public Sound(SoundAsset snd) {
        this.asset = snd;
    }

    public Sound loop(boolean l) {
        this.loop = l;
        return this.refresh();
    }

    public Sound refresh() {
        if (this.asset != null) {
            if (this.asset.getSound().isRunning()) {
                this.resume();
            }
        }
        return this;
    }

    public void play() {
        if (this.asset != null) {
            Clip snd = this.asset.getSound();
            snd.setMicrosecondPosition(0);
            snd.start();
        }
    }

    public void stop() {
        if (this.asset != null) {
            Clip snd = this.asset.getSound();
            snd.stop();
            snd.setMicrosecondPosition(0);
        }
    }

    public void resume() {
        if (this.asset != null) {
            Clip snd = this.asset.getSound();
            if (this.loop) {
                snd.loop(Clip.LOOP_CONTINUOUSLY);
            }
            if (!snd.isRunning()) {
                snd.start();
            }
        }
    }

    public void pause() {
        if (this.asset != null) {
            Clip snd = this.asset.getSound();
            if (snd.isRunning()) {
                snd.stop();
            }
        }
    }

    public Clip getSound() {
        return this.asset == null ? null : this.asset.getSound();
    }

    public Sound setVolume(double gain) {
        Clip snd = this.getSound();
        if (snd != null) {
            FloatControl gainControl = (FloatControl) snd
                    .getControl(FloatControl.Type.MASTER_GAIN);
            float dB = (float) (Math.log(gain) * 20.0);
            gainControl.setValue(dB);
        }
        return this;
    }

    public Sound clone() {
        return new Sound(this.asset.path);
    }

}
