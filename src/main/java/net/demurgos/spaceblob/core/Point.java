package net.demurgos.spaceblob.core;

/*
 *
 * An utility object for 2D mathematics.
 *
 */

public class Point {

    public static final Point I = new Point(0, 1);
    public static double precision = 1e-6;
    public double x;
    public double y;
    public double mod;
    public double arg;

    public Point(double a, double b, boolean polar) {
        if (polar) {
            this.setPolar(a, b);
        } else {
            this.setCartesian(a, b);
        }
    }

    public Point(double x, double y) {
        this.setCartesian(x, y);
    }

    public Point(double x) {
        this.setCartesian(x, 0);
    }

    public Point() {
        this.setCartesian(0, 0);
    }

    public static Point average(Point a, Point b) {
        return new Point((a.getX() + b.getX()) / 2, (a.getY() + b.getY()) / 2);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    public Point clone() {
        return new Point(this.x, this.y);
    }

    public boolean equals(Point p) {
        return Math.abs(this.x - p.getX()) < Point.precision
                && Math.abs(this.y - p.getY()) < Point.precision;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getMod() {
        return this.mod;
    }

    public double getArg() {
        return this.arg;
    }

    public Point setCartesian(double x, double y) {
        this.x = x;
        this.y = y;
        this.updatePolar();
        return this;
    }

    public Point setPolar(double mod, double arg) {
        this.mod = mod;
        this.arg = arg;
        this.updateCartesian();
        return this;
    }

    private void updatePolar() {
        this.mod = Math.sqrt(this.x * this.x + this.y * this.y);

        if (this.x == 0) {
            if (this.y == 0) {
                this.arg = 0;
            } else {
                this.arg = (this.y > 0 ? 1 : -1) * Math.PI / 2;
            }
        } else {
            this.arg = Math.atan(this.y / this.x);
            if (this.x < 0) {
                this.arg += (this.y > 0 ? 1 : -1) * Math.PI;
            }
        }
    }

    private void updateCartesian() {
        this.x = this.mod * Math.cos(this.arg);
        this.y = this.mod * Math.sin(this.arg);
    }

    public Point add(Point z) {
        return this.setCartesian(this.x + z.getX(), this.y + z.getY());
    }

    public Point add(double x, double y) {
        return this.add(new Point(x, y));
    }

    public Point sub(Point z) {
        return this.setCartesian(this.x - z.getX(), this.y - z.getY());
    }

    public Point sub(double x, double y) {
        return this.sub(new Point(x, y));
    }

    public Point mul(Point z) {
        return this.setPolar(this.mod * z.getMod(), this.arg + z.getArg());
    }

    public Point mul(double a) {
        return this.mul(new Point(a, 0));
    }

    public Point div(Point z) {
        return this.setPolar(this.mod / z.getMod(), this.arg - z.getArg());
    }

    public Point div(double a) {
        return this.div(new Point(a, 0));
    }

    public Point inv() {
        return this.setPolar(1 / this.mod, -this.arg);
    }

    public Point flipY() {
        return this.setCartesian(this.x, -this.y);
    }

    public Point flipX() {
        return this.setCartesian(-this.x, this.y);
    }

    public double dist(Point z) {
        return this.clone().sub(z).getMod();
    }

    public Point unit() {
        return this.setPolar(1, this.arg);
    }

}
