package net.demurgos.spaceblob;

import javax.swing.*;
import java.awt.*;

/*
 *
 * A Panel listening to the game rendering loop.
 *
 */

public class GameView extends JPanel {

    private static final long serialVersionUID = -1043693335166521135L;

    public Game game = null;

    GameView() {
        super();
        this.setPreferredSize(new Dimension(Game.WIDTH, Game.HEIGHT));
    }

    public void render(Game game) {
        this.game = game;
        this.game.bindView(this);
    }

    public void paintComponent(Graphics g) {
        int w = this.game.view.getWidth();
        int h = this.game.view.getHeight();
        Dimension s = this.getSize();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, s.width, s.height);
        g.drawImage(this.game.view, Math.max(s.width - w, 0) / 2,
                Math.max(s.height - h, 0) / 2, null);
    }

}
