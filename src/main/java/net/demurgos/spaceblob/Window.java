package net.demurgos.spaceblob;

import net.demurgos.spaceblob.assets.ImageAsset;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame {

	/*
     *
	 * The default window.
	 */

    private static final long serialVersionUID = 1L;
    public static ImageAsset ICON = new ImageAsset("img/icon.png");

    Window() {
        super();

        this.setTitle("Space Blob");
        this.setIconImage(Window.ICON.getImage());
        this.setSize(new Dimension(Game.WIDTH, Game.HEIGHT));
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
