package net.demurgos.spaceblob;

import net.demurgos.spaceblob.controls.KeyboardControls;

import javax.swing.*;

/*
 *
 * Main class: opens a window & starts the game.
 *
 */

public class Main {

    public static Window mainWindow;

    public static void main(String[] args) {

        mainWindow = new Window();

        GameView mainView = new GameView();
        Game game = new Game();
      KeyboardControls controls = new KeyboardControls();

      controls.bindTo(mainWindow);

      game.setControls(controls);

        mainView.render(game);
        setPane(mainView);
        mainWindow.pack();
        mainWindow.setVisible(true);
        game.run();
        game.play();

    }

    public static void setPane(JPanel panel) {
        JPanel contentPane = (JPanel) mainWindow.getContentPane();

        contentPane.removeAll();
        contentPane.add(panel);
        contentPane.revalidate();
        contentPane.repaint();
    }
}
