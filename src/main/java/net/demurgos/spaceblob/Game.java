package net.demurgos.spaceblob;

import net.demurgos.spaceblob.assets.Asset;
import net.demurgos.spaceblob.assets.LevelAsset;
import net.demurgos.spaceblob.assets.SaveAsset;
import net.demurgos.spaceblob.assets.SoundAsset;
import net.demurgos.spaceblob.controls.Controls;
import net.demurgos.spaceblob.core.Entity;
import net.demurgos.spaceblob.core.Layer;
import net.demurgos.spaceblob.core.Levels;
import net.demurgos.spaceblob.core.Sound;
import net.demurgos.spaceblob.layers.Menu;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.Timer;

/*
 *
 * Responsible for the timer (update, draw) calls & keep a trace of inputs & outputs.
 *
 */

public class Game {
    public static final int WIDTH = 640;
    public static final int HEIGHT = 480;
    public static final long TICK_DELAY = 15;
    public static SoundAsset MUSIC = new SoundAsset("snd/sw1.wav");

    public BufferedImage view = new BufferedImage(Game.WIDTH, Game.HEIGHT,
            BufferedImage.TYPE_INT_ARGB);
    public Graphics2D gfx = view.createGraphics();
    public BufferedImage nextView = new BufferedImage(Game.WIDTH, Game.HEIGHT,
            BufferedImage.TYPE_INT_ARGB);
    public Graphics2D nextGfx = nextView.createGraphics();
    public List<Sound> sounds = new ArrayList<Sound>();
    public List<JPanel> panels = new ArrayList<JPanel>();
    public Timer timer = new Timer();
    public long lastTime;
    public Controls controls;
    public boolean isPaused = false;
    public boolean isFullScreen = false;
    public Layer root;
    public Menu mainMenu;
    public Levels levels;
    public SaveAsset save = new SaveAsset("save.json");
    public Window fullScreenWindow;

    public Game() {
        Asset.loadAll();

        Sound snd = new Sound(Game.MUSIC);
        snd.loop(true);
        this.add(snd);
        snd.pause();
        snd.resume();

        this.root = new Layer(this);
        this.root.keep();
        this.root.enableEntityCache(true);
        this.mainMenu = new Menu(this);
        this.mainMenu.keep();
        this.root.add(this.mainMenu);

        // this.playSolo();
    }

    public void run() {
        this.play();
        final Game self = this;
        this.timer.schedule(new TimerTask() {
            public void run() {
                self.onTick();
            }
        }, 0, Game.TICK_DELAY);
    }

    public void onTick() {
        long newTime = (new Date()).getTime();
        int delta = (int) (newTime - this.lastTime);
        if (0 < delta && delta < 5 * Game.TICK_DELAY) {
            this.update(delta);
            this.draw();
            this.render();
        }
        this.lastTime = newTime;
    }

    public void update(int delta) {
        this.controls.captureControls();

        if (this.controls.isActivating("fullScreen")) {
            if (this.isFullScreen) {
                this.disableFullScreen();
            } else {
                this.enableFullScreen();
            }
        }

        if (this.controls.isActivating("pause")) {
            if (this.isPaused) {
                this.play();
            } else {
                this.pause();
            }
        }

        if (this.controls.isActivating("menu")) {
            Menu m = this.root.getFirstEntity(Menu.class);
            if (m == null) {
                for (Entity e : this.root.children) {
                    e.dispose();
                }
                this.mainMenu.keep();
                this.root.add(this.mainMenu);
            }
        }

        if (this.isPaused) {
            return;
        }

        this.root.update(delta);

    }

    public void draw() {
        AffineTransform oldMatrix = this.nextGfx.getTransform();
        double ratio = ((double) this.nextView.getWidth())
                / ((double) Game.WIDTH);
        this.nextGfx.scale(ratio, ratio);
        this.root.draw(this.nextGfx);
        this.nextGfx.setTransform(oldMatrix);
        this.swapViews();
    }

    public void swapViews() {
        BufferedImage tmpView = this.view;
        Graphics2D tmpGfx = this.gfx;
        this.view = this.nextView;
        this.gfx = this.nextGfx;

        int bestWidth = Game.WIDTH;

        for (JPanel p : this.panels) {
            if (SwingUtilities.getWindowAncestor(p).isVisible()) {
                Dimension s = p.getSize();
                int maxWidth = Math.min(s.width, s.height * Game.WIDTH
                        / Game.HEIGHT);
                if (maxWidth > bestWidth) {
                    bestWidth = maxWidth;
                }
            }
        }

        if (bestWidth != tmpView.getWidth()) {
            this.nextView = new BufferedImage(bestWidth, bestWidth * Game.WIDTH
                    / Game.HEIGHT, BufferedImage.TYPE_INT_ARGB);
            this.nextGfx = nextView.createGraphics();
        } else {
            this.nextView = tmpView;
            this.nextGfx = tmpGfx;
        }

    }

    public void bindView(JPanel view) {
        this.panels.add(view);
    }

    public Window getFullScreenWindow() {
        if (this.fullScreenWindow != null) {
            return this.fullScreenWindow;
        }

        GameView fullScreenView = new GameView();
        fullScreenView.render(this);

        this.fullScreenWindow = new Window();
        this.fullScreenWindow.setUndecorated(true);
        this.fullScreenWindow.setAlwaysOnTop(true);
        this.fullScreenWindow.setContentPane(fullScreenView);
        this.fullScreenWindow.pack();
        this.fullScreenWindow.setVisible(true);

        return this.fullScreenWindow;
    }

    public void enableFullScreen() {
        this.isFullScreen = true;
        GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
        GraphicsDevice[] gdArray = ge.getScreenDevices();

        GraphicsDevice gd = null;

        for (int i = 0; i < gdArray.length; i++) {
            if (gdArray[i].isFullScreenSupported()) {
                gd = gdArray[i];
                break;
            }
        }

        if (gd == null) {
            return;
        }

        for (JPanel p : this.panels) {
            SwingUtilities.getWindowAncestor(p).setVisible(false);
        }

        Window w = this.getFullScreenWindow();
        w.setVisible(true);
        gd.setFullScreenWindow(w);

    }

    public void disableFullScreen() {
        this.isFullScreen = false;

        GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
        GraphicsDevice[] gdArray = ge.getScreenDevices();

        GraphicsDevice gd = null;

        for (int i = 0; i < gdArray.length; i++) {
            if (gdArray[i].isFullScreenSupported()) {
                gd = gdArray[i];
                break;
            }
        }

        if (gd == null) {
            return;
        }

        gd.setFullScreenWindow(null);

        for (JPanel p : this.panels) {
            Window w = (Window) SwingUtilities.getWindowAncestor(p);
            if (w != null) {
                if (w == this.fullScreenWindow) {
                    w.setVisible(false);
                } else {
                    w.setVisible(true);
                }
            }
        }
    }

    public void setControls(Controls controls) {
        this.controls = controls;
    }

    public void render() {
        for (JPanel p : this.panels) {
            if (SwingUtilities.getWindowAncestor(p).isVisible()) {
                p.repaint();
            }
        }
    }

    public void play() {
        this.isPaused = false;
        this.lastTime = (new Date()).getTime();
        for (Sound s : this.sounds) {
            s.resume();
        }
    }

    public void pause() {
        this.isPaused = true;
        for (Sound s : this.sounds) {
            s.pause();
        }
    }

    public Game add(Sound s) {
        this.sounds.add(s);
        if (!this.isPaused) {
            s.play();
        }
        return this;
    }

    // jsonObject.getJSONArray("layers")[0].get("name");
    public void playSolo(int lvl) {
        List<LevelAsset> lla = new ArrayList<LevelAsset>();
        lla.add(new LevelAsset("lvl/100.json"));
        lla.add(new LevelAsset("lvl/101.json"));
        lla.add(new LevelAsset("lvl/102.json"));
        lla.add(new LevelAsset("lvl/103.json"));
        lla.add(new LevelAsset("lvl/001.json"));
        lla.add(new LevelAsset("lvl/005.json"));
        lla.add(new LevelAsset("lvl/006.json"));
        lla.add(new LevelAsset("lvl/000.json"));
        lla.add(new LevelAsset("lvl/003.json"));
        lla.add(new LevelAsset("lvl/104.json"));

        this.levels = new Levels(this, lla);
        this.levels.load(lvl);
    }
}
