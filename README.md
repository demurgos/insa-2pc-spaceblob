# Space blob

Space blob is a small platformer game I made for a school assignment,
most notably it contains a custom rendering and physical engine.

## Build

- **Minimal Java version**: 1.8
- **Source directory**: `src/` (main class: `if2a.Main`)
- **JAR dependencies**:
  - `lib/jinput/jinput.jar`
  - `lib/json-simple/json-simple-1.1.jar`
- **External libraries (DLL) directories**:
  - `lib/jinput/`

```
mvn install
mvn exec:java
```

## Licence

[MIT License](./LICENSE.txt)
